import Vue from 'vue'
import Vuex from 'vuex'

import auth from '@/modules/auth/store'
import list from '@/modules/list/store'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    list
  }
})
